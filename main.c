#include "xml/xml.h"

int main(void)
{
  xml_s *document = xmlLoadFile("love-shield.svg");
  xmlSaveFile(document, "output.xml");
  xmlDelete(document);
  return 0;
}
